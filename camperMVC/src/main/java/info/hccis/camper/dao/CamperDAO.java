package info.hccis.camper.dao;

import info.hccis.camper.entity.Camper;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.logging.Logger;

/**
 * This class will contain db functionality for working with Campers
 *
 * @author bjmaclean
 * @since 20160929
 */
public class CamperDAO {

    private final static Logger LOGGER = Logger.getLogger(CamperDAO.class.getName());

    public static Camper select(int idIn) {

        PreparedStatement ps = null;
        String sql = null;
        Connection conn = null;
        Camper theCamper = null;
        try {
            conn = ConnectionUtils.getConnection();

            sql = "SELECT * FROM camper where id = ?";

            ps = conn.prepareStatement(sql);
            ps.setInt(1, idIn);
            ResultSet rs = ps.executeQuery();

            while (rs.next()) {

                int id = rs.getInt("id");
                String firstName = rs.getString("firstName");
                String lastName = rs.getString("lastName");
                String dob = rs.getString("dob");
                theCamper = new Camper(id, firstName, lastName, dob);
            }
        } catch (Exception e) {
            String errorMessage = e.getMessage();
            e.printStackTrace();
        } finally {
            DbUtils.close(ps, conn);
        }
        return theCamper;
    }

    /**
     * Delete a camper based on id
     * @param idIn 
     * @since 20191011
     * @author cis2232 Summerside
     */
    public static void delete(int idIn) {

        PreparedStatement ps = null;
        String sql = null;
        Connection conn = null;
        Camper theCamper = null;
        try {
            conn = ConnectionUtils.getConnection();

            sql = "DELETE FROM Camper WHERE id = ?";

            ps = conn.prepareStatement(sql);
            ps.setInt(1, idIn);
            ps.executeUpdate();

        } catch (Exception e) {
            String errorMessage = e.getMessage();
            e.printStackTrace();
        } finally {
            DbUtils.close(ps, conn);
        }
    }

    /**
     * Select all campers
     *
     * @author bjmaclean
     * @since 20160929
     * @return
     */
    public static ArrayList<Camper> selectAll() {

        ArrayList<Camper> campers = new ArrayList();
        PreparedStatement ps = null;
        String sql = null;
        Connection conn = null;
        try {
            conn = ConnectionUtils.getConnection();

            sql = "SELECT * FROM camper  order by id";

            ps = conn.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {

                int id = rs.getInt("id");
                String firstName = rs.getString("firstName");
                String lastName = rs.getString("lastName");
                String dob = rs.getString("dob");
                campers.add(new Camper(id, firstName, lastName, dob));
            }
        } catch (Exception e) {
            String errorMessage = e.getMessage();
            e.printStackTrace();
        } finally {
            DbUtils.close(ps, conn);
        }
        return campers;
    }

    public static ArrayList<Camper> selectAllByDOB(String year) {

        ArrayList<Camper> campers = new ArrayList();
        PreparedStatement ps = null;
        String sql = null;
        Connection conn = null;
        try {
            conn = ConnectionUtils.getConnection();

            sql = "SELECT * FROM camper  where dob like ? order by id";

            ps = conn.prepareStatement(sql);
            ps.setString(1, year + "%");
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {

                int id = rs.getInt("id");
                String firstName = rs.getString("firstName");
                String lastName = rs.getString("lastName");
                String dob = rs.getString("dob");
                campers.add(new Camper(id, firstName, lastName, dob));
            }
        } catch (Exception e) {
            String errorMessage = e.getMessage();
            e.printStackTrace();
        } finally {
            DbUtils.close(ps, conn);
        }
        return campers;
    }

    /**
     * This method will insert.
     *
     * @return
     * @author BJ
     * @since 20140615
     */
    public static synchronized Camper update(Camper camper) throws Exception {
//        System.out.println("inserting camper");
        PreparedStatement ps = null;
        String sql = null;
        Connection conn = null;


        /*
         * Setup the sql to update or insert the row.
         */
        try {
            conn = ConnectionUtils.getConnection();

            //Check to see if camper exists.
            if (camper.getId()== null) {

                sql = "SELECT max(id) from Camper";
                ps = conn.prepareStatement(sql);
                ResultSet rs = ps.executeQuery();
                int max = 0;
                while (rs.next()) {
                    max = rs.getInt(1) + 1;
                }

                camper.setId(max);

                sql = "INSERT INTO `Camper`(`id`, `firstName`, `lastName`, `dob`) "
                        + "VALUES (?,?,?,?)";

                ps = conn.prepareStatement(sql);
                ps.setInt(1, camper.getId());
                ps.setString(2, camper.getFirstName());
                ps.setString(3, camper.getLastName());
                ps.setString(4, camper.getDob());

            } else {

                sql = "UPDATE `Camper` SET `firstName`=?,`lastName`=?,`dob`=? WHERE id = ?";

                ps = conn.prepareStatement(sql);
                ps.setString(1, camper.getFirstName());
                ps.setString(2, camper.getLastName());
                ps.setString(3, camper.getDob());
                ps.setInt(4, camper.getId());

            }
            /*
             Note executeUpdate() for update vs executeQuery for read only!!
             */

            ps.executeUpdate();

        } catch (Exception e) {
            String errorMessage = e.getMessage();
            e.printStackTrace();
            throw e;
        } finally {
            DbUtils.close(ps, conn);
        }
        return camper;

    }

}

//    /**
//     * This method will insert.
//     *
//     * @return
//     * @author BJ
//     * @since 20140615
//     */
//    public static void insertNotification(Notification notification) throws Exception {
//        System.out.println("inserting notification");
//        PreparedStatement ps = null;
//        String sql = null;
//        Connection conn = null;
//
//        /*
//         * Setup the sql to update or insert the row.
//         */
//        try {
//            conn = ConnectionUtils.getConnection();
//
//            sql = "INSERT INTO `notification`(`notification_type_code`,  "
//                    + "`notification_detail`, `status_code`, `created_date_time`, "
//                    + "`created_user_id`, `updated_date_time`, `updated_user_id`) "
//                    + "VALUES (?, ?, 1, sysdate(), ?, sysdate(), ?)";
//
//            ps = conn.prepareStatement(sql);
//            ps.setInt(1, notification.getNotificationType());
//            ps.setString(2, notification.getNotificationDetail());
//            ps.setString(3, notification.getUserId());
//            ps.setString(4, notification.getUserId());
//
//            ps.executeUpdate();
//
//        } catch (Exception e) {
//            String errorMessage = e.getMessage();
//            e.printStackTrace();
//            throw e;
//        } finally {
//            DbUtils.close(ps, conn);
//        }
//        return;
//
//    }
//
//    /**
//     * Delete the specified member education (set to inactive)
//     * @param memberId
//     * @param memberEducationSequence 
//     */
//    public static void deleteNotification(int notificationId) throws Exception{
//        
//        System.out.println("deleting notification");
//        PreparedStatement ps = null;
//        String sql = null;
//        Connection conn = null;
//
//        /*
//         * Setup the sql to update or insert the row.
//         */
//        try {
//            conn = ConnectionUtils.getConnection();
//
//            sql = "update notification set status_code = 0, updated_date_time = sysdate() "
//                + "where notification_id = ? ";
//
//            ps = conn.prepareStatement(sql);
//            ps.setInt(1, notificationId);
//
//            ps.executeUpdate();
//
//        } catch (Exception e) {
//            String errorMessage = e.getMessage();
//            e.printStackTrace();
//            throw e;
//        } finally {
//            DbUtils.close(ps, conn);
//        }
//        return;
//
//    }
//        
//
//    
//    public static ArrayList<Notification> getNotifications() {
//        ArrayList<Notification> notifications = new ArrayList();
//        PreparedStatement ps = null;
//        String sql = null;
//        Connection conn = null;
//        try {
//            conn = ConnectionUtils.getConnection();
//
//            sql = "SELECT * FROM notification WHERE status_code = 1 order by created_date_time desc";
//
//            ps = conn.prepareStatement(sql);
//            ResultSet rs = ps.executeQuery();
//            while (rs.next()) {
//                Notification newNotification = new Notification();
//                newNotification.setNotificationId(rs.getInt("notification_id"));
//                newNotification.setNotificationDetail(rs.getString("notification_detail"));
//                newNotification.setNotificationType(rs.getInt("notification_type_code"));
//                newNotification.setNotificationDate(rs.getString("created_date_time"));
//                newNotification.setUserId(rs.getString("created_user_id"));
//                notifications.add(newNotification);
//            }
//        } catch (Exception e) {
//            String errorMessage = e.getMessage();
//            e.printStackTrace();
//        } finally {
//            DbUtils.close(ps, conn);
//        }
//        return notifications;
//    }

