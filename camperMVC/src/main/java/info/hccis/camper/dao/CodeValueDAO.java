package info.hccis.camper.dao;


import info.hccis.camper.jpa.CodeValue;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author BJ
 */
public class CodeValueDAO {
//public static void loadCodes(HttpServletRequest request){
//    
//    request.getSession().setAttribute("organizations",getCodeValues("1"));
//    request.getSession().setAttribute("statuses",getCodeValues("2"));
//    request.getSession().setAttribute("salutations",getCodeValues("3"));
//    request.getSession().setAttribute("circulation_groups",getCodeValues("4"));
//    request.getSession().setAttribute("provinces",getCodeValues("5"));
//    request.getSession().setAttribute("genders",getCodeValues("6"));
//    request.getSession().setAttribute("countries",getCodeValues("7"));
//    request.getSession().setAttribute("programs",getCodeValues("8"));
//    request.getSession().setAttribute("employment_statuses",getCodeValues("9"));
//    request.getSession().setAttribute("currencies",getCodeValues("10"));
//    request.getSession().setAttribute("practice_areas",getCodeValues("11"));
//    request.getSession().setAttribute("employment_statuses",getCodeValues("12"));
//    request.getSession().setAttribute("employment_categories",getCodeValues("13"));
//    request.getSession().setAttribute("funding_sources",getCodeValues("14"));
//    request.getSession().setAttribute("positions",getCodeValues("15"));
//    request.getSession().setAttribute("notification_types",getCodeValues("16"));
//    request.getSession().setAttribute("user_types",getCodeValues("17"));
//    
//    return;
//}    
//
//public static CodeValue getCodeValueFromSession(HttpServletRequest request, String collectionName, int codeSequenceValue){
//    
//    ArrayList<CodeValue> theList = (ArrayList<CodeValue>) request.getSession().getAttribute(collectionName);
//    boolean found = false;
//    int location = 0;
//    CodeValue theCodeValue = null;
//    while (!found && location < theList.size()){
//        if (theList.get(location).getCodeValueSequence() == codeSequenceValue){
//            found = true;
//            theCodeValue = theList.get(location);
//        }
//        location++;
//    }
//    
//    return theCodeValue;
//} 

    public static ArrayList<CodeValue> getCodeValues(DatabaseConnection databaseConnection, String codeTypeId) {
        ArrayList<CodeValue> codes = new ArrayList();

        PreparedStatement ps = null;
        String sql = null;
        Connection conn = null;

        try {
            conn = ConnectionUtils.getConnection();

            sql = "SELECT * FROM `CodeValue` WHERE codeTypeId = " + codeTypeId;

            ps = conn.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                // It is possible to get the columns via name
                // also possible to get the columns via the column number
                // which starts at 1

                // e.g. resultSet.getSTring(2);
                CodeValue codeValue = new CodeValue();
                codeValue.setCodeTypeId(Integer.parseInt(codeTypeId));
                codeValue.setCodeValueSequence(rs.getInt("codeValueSequence"));
                codeValue.setEnglishDescription(rs.getString("englishDescription"));
                codeValue.setEnglishDescriptionShort(rs.getString("englishDescriptionShort"));
                codes.add(codeValue);
            }
        } catch (Exception e) {
            String errorMessage = e.getMessage();
            e.printStackTrace();
        } finally {
            DbUtils.close(ps, conn);
        }

        return codes;

    }

    public static CodeValue getCodeValue(DatabaseConnection databaseConnection, String codeTypeId, String codeValueSequence) {
        PreparedStatement ps = null;
        String sql = null;
        Connection conn = null;
        CodeValue codeValue = new CodeValue();

        try {
            conn = ConnectionUtils.getConnection();

            sql = "SELECT * FROM `CodeValue` WHERE codeTypeId = " + codeTypeId
                    + " and codeValueSequence=?";

            ps = conn.prepareStatement(sql);
            ps.setInt(1, Integer.parseInt(codeValueSequence));
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                // It is possible to get the columns via name
                // also possible to get the columns via the column number
                // which starts at 1

                // e.g. resultSet.getSTring(2);
                codeValue.setCodeTypeId(Integer.parseInt(codeTypeId));
                codeValue.setCodeValueSequence(rs.getInt("codeValueSequence"));
                codeValue.setEnglishDescription(rs.getString("englishDescription"));
                codeValue.setEnglishDescriptionShort(rs.getString("englishDescriptionShort"));
            }
        } catch (Exception e) {
            String errorMessage = e.getMessage();
            e.printStackTrace();
        } finally {
            DbUtils.close(ps, conn);
        }

        return codeValue;

    }

    public static String getCodeValueDescription(DatabaseConnection databaseConnection, int codeTypeId, int sequence) {
        PreparedStatement ps = null;
        String sql = null;
        Connection conn = null;
        String description = "";
        try {
            conn = ConnectionUtils.getConnection();

            sql = "SELECT * FROM `CodeValue` WHERE codeTypeId = " + codeTypeId + " and codeValueSequence = " + sequence;

            ps = conn.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                description = rs.getString("englishDescription");
            }
        } catch (Exception e) {
            String errorMessage = e.getMessage();
            e.printStackTrace();
        } finally {
            DbUtils.close(ps, conn);
        }

        return description;

    }

    public static void addUpdate(DatabaseConnection databaseConnection, CodeValue codeValueToAdd) {
        try {
            PreparedStatement ps = null;
            String sql = null;
            Connection conn = null;
            int maxID = 0;
            boolean adding = false;
            try {
                conn = ConnectionUtils.getConnection();

                if (codeValueToAdd.getCodeValueSequence() == 0) {
                    adding = true;
                    sql = "SELECT max(codeValueSequence) FROM `CodeValue` ";

                    ps = conn.prepareStatement(sql);
                    ResultSet rs = ps.executeQuery();
                    if (rs.next()) {
                        codeValueToAdd.setCodeValueSequence((rs.getInt(1) + 1));
                    }
                }
            } catch (Exception e) {
                String errorMessage = e.getMessage();
                e.printStackTrace();
            }

            if (adding) {

                sql = "INSERT INTO `CodeValue`(`codeTypeId`, `codeValueSequence`, "
                        + "`englishDescription`, `englishDescriptionShort`, `frenchDescription`,"
                        + " `frenchDescriptionShort`, `createdDateTime`, `createdUserId`, "
                        + "`updatedDateTime`, `updatedUserId`) "
                        + "VALUES (?,?,?,?,?,?,sysdate(),'admin',sysdate(),'admin')";

                ps = conn.prepareStatement(sql);
                ps.setInt(1, codeValueToAdd.getCodeTypeId());
                ps.setInt(2, codeValueToAdd.getCodeValueSequence());
                ps.setString(3, codeValueToAdd.getEnglishDescription());
                ps.setString(4, codeValueToAdd.getEnglishDescriptionShort());
                ps.setString(5, codeValueToAdd.getEnglishDescription()+ "FR");
                ps.setString(6, codeValueToAdd.getEnglishDescriptionShort()+ "FR");
            } else {
                sql = "UPDATE CodeValue SET englishDescription=?,englishDescriptionShort=?,frenchDescription=?,frenchDescriptionShort=? WHERE codeTypeId = ? and codeValueSequence=?";
                ps = conn.prepareStatement(sql);
                ps.setString(1, codeValueToAdd.getEnglishDescription());
                ps.setString(2, codeValueToAdd.getEnglishDescriptionShort());
                ps.setString(3, codeValueToAdd.getEnglishDescription()+ "FR");
                ps.setString(4, codeValueToAdd.getEnglishDescriptionShort()+ "FR");
                ps.setInt(5, codeValueToAdd.getCodeTypeId());
                ps.setInt(6, codeValueToAdd.getCodeValueSequence());

            }

            ps.executeUpdate();

            DbUtils.close(ps, conn);
        } catch (SQLException ex) {
            Logger.getLogger(CodeValueDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * This method will delete the row that the user wants to delete.
     *
     * @author Zyler Fenner
     * @since 20151002
     *
     * @param codeType
     * @param codeValue
     */
    public static void delete(DatabaseConnection databaseConnection, int codeTypeId, int codeValueSequence) {
        PreparedStatement ps = null;
        String sql = null;
        Connection conn = null;

        try {
            conn = ConnectionUtils.getConnection();

            sql = "DELETE FROM `CodeValue` "
                    + "WHERE `codeTypeId` = " + codeTypeId
                    + "AND 'codeValueSequence' = " + codeValueSequence;

            ps = conn.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
        } catch (Exception e) {
            String errorMessage = e.getMessage();
            e.printStackTrace();
        } finally {
            DbUtils.close(ps, conn);
        }
    }

    /**
     * @author Connor Oliver
     * @param searchTerm
     * @return
     */
    public static ArrayList<CodeValue> find(DatabaseConnection databaseConnection, String searchTerm) {
        //Connor Oliver
        ArrayList<CodeValue> codes = new ArrayList();

        PreparedStatement ps = null;
        String sql = null;
        Connection conn = null;

        try {
            conn = ConnectionUtils.getConnection();

            sql = "SELECT * FROM CodeValue WHERE englishDescription like '" + searchTerm + "'";

            ps = conn.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                codes.add(new CodeValue(rs.getInt("codeTypeId"),
                        rs.getInt("codeValueSequence"), rs.getString("englishDescription"),
                        rs.getString("englishDescriptionShort")
                ));
            }
        } catch (Exception e) {
            String errorMessage = e.getMessage();
            e.printStackTrace();
        } finally {
            DbUtils.close(ps, conn);
        }

        return codes;
    }

}
