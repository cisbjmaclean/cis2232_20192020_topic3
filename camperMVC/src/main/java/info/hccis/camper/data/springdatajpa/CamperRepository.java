package info.hccis.camper.data.springdatajpa;

import info.hccis.camper.jpa.Camper;
import info.hccis.camper.jpa.UserAccess;
import java.util.List;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CamperRepository extends CrudRepository<Camper, Integer> {

    List<UserAccess> findByFirstName(String firstName);

}
