drop database canes;
create database canes;
use canes;

--
-- Database: `canes`
--

-- --------------------------------------------------------

--
-- Table structure for table `camper`
--

CREATE TABLE `Camper` (
  `id` int(4) UNSIGNED AUTO_INCREMENT NOT NULL COMMENT 'Registration id',
  `firstName` varchar(10) NOT NULL,
  `lastName` varchar(10) NOT NULL,
  `dob` varchar(10) DEFAULT NULL,
  campType int(3) DEFAULT 0,
PRIMARY KEY (`id`)
);

--
-- Dumping data for table `camper`
--

INSERT INTO `Camper` (`id`, `firstName`, `lastName`, `dob`,campType) VALUES
(1, 'Nick', 'Matheson', '12345',2);
INSERT INTO `Camper` (`id`, `firstName`, `lastName`, `dob`,campType) VALUES
(2, 'Audrey', 'Matheson', '12345',2);
INSERT INTO `Camper` (`id`, `firstName`, `lastName`, `dob`,campType) VALUES
(3, 'Patrick', 'Campbell', '12345',2);
INSERT INTO `Camper` (`id`, `firstName`, `lastName`, `dob`,campType) VALUES
(4, 'Nick', 'Smith', '12345',2);

-- --------------------------------------------------------

--
-- Table structure for table `CodeType`
--

CREATE TABLE `CodeType` (
  `CodeTypeId` int(3) NOT NULL COMMENT 'This is the primary key for code types',
  `englishDescription` varchar(100) NOT NULL COMMENT 'English description',
  `frenchDescription` varchar(100) DEFAULT NULL COMMENT 'French description',
  `createdDateTime` datetime DEFAULT NULL,
  `createdUserId` varchar(20) DEFAULT NULL,
  `updatedDateTime` datetime DEFAULT NULL,
  `updatedUserId` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='This hold the code types that are available for the applicat';

--
-- Dumping data for table `CodeType`
--

INSERT INTO `CodeType` (`CodeTypeId`, `englishDescription`, `frenchDescription`, `createdDateTime`, `createdUserId`, `updatedDateTime`, `updatedUserId`) VALUES
(1, 'User Types', 'User Types FR', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '');

-- --------------------------------------------------------

--
-- Table structure for table `CodeValue`
--

CREATE TABLE `CodeValue` (
  `codeTypeId` int(3) NOT NULL COMMENT 'see code_type table',
  `codeValueSequence` int(3) NOT NULL,
  `englishDescription` varchar(100) NOT NULL COMMENT 'English description',
  `englishDescriptionShort` varchar(20) NOT NULL COMMENT 'English abbreviation for description',
  `frenchDescription` varchar(100) DEFAULT NULL COMMENT 'French description',
  `frenchDescriptionShort` varchar(20) DEFAULT NULL COMMENT 'French abbreviation for description',
  `createdDateTime` datetime DEFAULT NULL,
  `createdUserId` varchar(20) DEFAULT NULL,
  `updatedDateTime` datetime DEFAULT NULL,
  `updatedUserId` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='This will hold code values for the application.';

--
-- Dumping data for table `CodeValue`
--

INSERT INTO `CodeValue` (`codeTypeId`, `codeValueSequence`, `englishDescription`, `englishDescriptionShort`, `frenchDescription`, `frenchDescriptionShort`, `createdDateTime`, `createdUserId`, `updatedDateTime`, `updatedUserId`) VALUES
(1, 1, 'General', 'General', 'GeneralFR', 'GeneralFR', '2015-10-25 18:44:37', 'admin', '2015-10-25 18:44:37', 'admin');
INSERT INTO `CodeValue` (`codeTypeId`, `codeValueSequence`, `englishDescription`, `englishDescriptionShort`, `frenchDescription`, `frenchDescriptionShort`, `createdDateTime`, `createdUserId`, `updatedDateTime`, `updatedUserId`) VALUES
(2, 2, 'Summer', 'Summer', 'Summer', 'Summer', '2015-10-25 18:44:37', 'admin', '2015-10-25 18:44:37', 'admin');

INSERT INTO `CodeValue` (`codeTypeId`, `codeValueSequence`, `englishDescription`, `englishDescriptionShort`, `frenchDescription`, `frenchDescriptionShort`, `createdDateTime`, `createdUserId`, `updatedDateTime`, `updatedUserId`) VALUES
(2, 3, 'PD Day', 'PD Day', 'PD Day', 'PD Day', '2015-10-25 18:44:37', 'admin', '2015-10-25 18:44:37', 'admin');
-- --------------------------------------------------------

--
-- Table structure for table `useraccess`
--

CREATE TABLE `UserAccess` (
  `userAccessId` int(3) NOT NULL,
  `username` varchar(100) NOT NULL COMMENT 'Unique user name for app',
  `password` varchar(128) NOT NULL,
  `userTypeCode` int(3) NOT NULL DEFAULT '1' COMMENT 'Code type #1',
  `createdDateTime` datetime DEFAULT NULL COMMENT 'When user was created.'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `CodeType`
--
ALTER TABLE `CodeType`
  ADD PRIMARY KEY (`CodeTypeId`);

--
-- Indexes for table `CodeValue`
--
ALTER TABLE `CodeValue`
  ADD PRIMARY KEY (`codeValueSequence`);
--  ADD KEY `codeTypeId` (`codeTypeId`);

--
-- Indexes for table `useraccess`
--
ALTER TABLE `useraccess`
  ADD PRIMARY KEY (`userAccessId`),
  ADD KEY `userTypeCode` (`userTypeCode`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `CodeType`
--
ALTER TABLE `CodeType`
  MODIFY `CodeTypeId` int(3) NOT NULL AUTO_INCREMENT COMMENT 'This is the primary key for code types', AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `CodeValue`
--
ALTER TABLE `CodeValue`
  MODIFY `codeValueSequence` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `useraccess`
--
ALTER TABLE `useraccess`
  MODIFY `userAccessId` int(3) NOT NULL AUTO_INCREMENT;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `CodeValue`
--
-- ALTER TABLE `CodeValue`
--   ADD CONSTRAINT `codevalue_ibfk_1` FOREIGN KEY (`codeTypeId`) REFERENCES `CodeType` (`CodeTypeId`);

INSERT INTO `useraccess` (`userAccessId`, `username`, `password`, `userTypeCode`, `createdDateTime`) VALUES
(1, 'bjmaclean', '202cb962ac59075b964b07152d234b70', 1, '2019-10-04 10:18:40');
