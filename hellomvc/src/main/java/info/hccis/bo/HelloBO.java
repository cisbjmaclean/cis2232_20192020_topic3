package info.hccis.bo;

public class HelloBO {

    /**
     * @author bjm
     * @since 20191004
     * Do something but not much.
     */
    public static void doSomething() {
        System.out.println("Hello from the business layer");
        System.out.println("Is Divya's still broke?");
    }

}
