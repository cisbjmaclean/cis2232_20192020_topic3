package info.hccis.bo;

/**
 * User POJO
 * @author bjmaclean
 * @since 20191004
 */
public class User {
    
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    
    
}
