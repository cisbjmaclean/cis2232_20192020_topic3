package info.hccis.web;

import info.hccis.bo.HelloBO;
import info.hccis.bo.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class HelloController {

    @RequestMapping("/")
    public String showHome(Model model) {

        //Put the object in the model so it will be available when the page renders
        User user = new User();
        user.setName("Joe");
        model.addAttribute("user", user);  //should be in the model when we get to the page.

        HelloBO.doSomething();

        //This will send the user to the welcome.html page.
        return "hello/welcome";
    }

    @RequestMapping("/newOne")
    public String showNewOne(Model model) {
        System.out.println("in controller for /newOne");
        return "hello/newOne";
    }

    @RequestMapping("/newTwo")
    public String showNewTwo(Model model, @ModelAttribute("user") User user) {
        System.out.println("in controller for /newTwo the user's name is " + user.getName());
        System.out.println("Now sending the user to a different view called newTwo");
        return "hello/newTwo";
    }

}
