drop database cis2232_camper;
create database cis2232_camper;
use cis2232_camper;

/*create a user in database*/
grant select, insert, update, delete, alter on cis2232_camper.*
to 'cis2232_admin'@'localhost'
identified by 'Test1234';
flush privileges;

drop table if exists Camper;

-- create instructor table and set primary key
-- no foreign keys set yet
CREATE TABLE Camper (
	registrationId int(5) PRIMARY KEY,
	firstName 	varchar(20),
	lastName 	varchar(50), 
	dob 	varchar(50)
	);	


INSERT INTO cis2232_camper.Camper VALUES ('1', 'BJ', 'MacLean', '20000115');